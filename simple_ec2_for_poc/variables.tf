variable "aws_region" {
  type = string
}

variable "aws_profile_name" {
  type = string
}

variable "maintainer" {
  type = string
}

variable "key_name" {
  default = "terraform_key"
  type = string
}