data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_key_pair" "my_key" {
  key_name   = var.key_name
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "aws_security_group" "allow_all_sg" {
  name        = "allow_all"
  description = "Allow All Traffic to temporarily Do POC"

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_instance" "ubuntu_instance" {
  ami                    = data.aws_ami.ubuntu.image_id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.allow_all_sg.id]
  key_name               = aws_key_pair.my_key.key_name
}