import {ListQueuesCommand, paginateListQueues, SendMessageCommand, SQSClient} from "@aws-sdk/client-sqs";


const client = new SQSClient({region: "ap-southeast-2"});


// Example ListQueue

// let queueURLs = [];
//
// await client.send(
//     new ListQueuesCommand( {
//         QueueNamePrefix: "terraform"
//     } )).then( (res) => {
//         console.log(res.QueueUrls)
//         queueURLs = res.QueueUrls;
//     }).catch( (e) => {
//         console.log(e)
//     });

let queueURL = "https://sqs.ap-southeast-2.amazonaws.com/<AWS_ACCOUNT>/terraform-example-queue";
console.log(queueURL)

await client.send(
    new SendMessageCommand({
        QueueUrl: queueURL,
        MessageBody: "Here's Message from my Laptop"
    })).then( (res) => {
        console.log(res)
    }
)


