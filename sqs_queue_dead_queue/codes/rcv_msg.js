import {ReceiveMessageCommand, SendMessageCommand, SQSClient} from "@aws-sdk/client-sqs";

const client = new SQSClient({region: "ap-southeast-2"});

await client.send(
    new ReceiveMessageCommand(
        {
            QueueUrl: "https://sqs.ap-southeast-2.amazonaws.com/<AWS_ACCOUNT>/terraform-example-queue",
            MaxNumberOfMessages: 2,
            VisibilityTimeout: 60
        }
    )
).then( (res) => {
        console.log(res)
    }
)
