import { SQSClient, SendMessageBatchCommand } from "@aws-sdk/client-sqs";

const client = new SQSClient({region: "ap-southeast-2"});


await client.send(new SendMessageBatchCommand({

    QueueUrl: "https://sqs.ap-southeast-2.amazonaws.com/<AWS_ACCOUNT>/terraform-example-queue",
    Entries: [
        { Id: 0, MessageBody: "Batch0",  },
        { Id: 1, MessageBody: "Batch1" },
        { Id: 2, MessageBody: "Batch2" },
        { Id: 3, MessageBody: "Batch3" }
    ]
})).then((res) => {
    console.log(res);
});





// { // SendMessageBatchResult
//   Successful: [ // SendMessageBatchResultEntryList // required
//     { // SendMessageBatchResultEntry
//       Id: "STRING_VALUE", // required
//       MessageId: "STRING_VALUE", // required
//       MD5OfMessageBody: "STRING_VALUE", // required
//       MD5OfMessageAttributes: "STRING_VALUE",
//       MD5OfMessageSystemAttributes: "STRING_VALUE",
//       SequenceNumber: "STRING_VALUE",
//     },
//   ],
//   Failed: [ // BatchResultErrorEntryList // required
//     { // BatchResultErrorEntry
//       Id: "STRING_VALUE", // required
//       SenderFault: true || false, // required
//       Code: "STRING_VALUE", // required
//       Message: "STRING_VALUE",
//     },
//   ],
// };

// {
//   '$metadata': {
//     httpStatusCode: 200,
//     requestId: '8f05a886-4ddb-57e6-81cc-f5ae52161752',
//     extendedRequestId: undefined,
//     cfId: undefined,
//     attempts: 1,
//     totalRetryDelay: 0
//   },
//   Successful: [
//     {
//       Id: '0',
//       MessageId: '9e5b1583-5076-49b5-9469-a025890d3499',
//       MD5OfMessageBody: '54baf7b367e0f728534c26c4f088252d'
//     },
//     {
//       Id: '1',
//       MessageId: '0d012167-844c-4579-b9f2-ea3eeebc64b2',
//       MD5OfMessageBody: 'e165522c27a31d9db7cfb792b3b79d2e'
//     },
//     {
//       Id: '2',
//       MessageId: '3201f2c1-1249-458d-a5f7-71d196405919',
//       MD5OfMessageBody: 'bda2bae874f0b72b258bee6ddbfe4612'
//     },
//     {
//       Id: '3',
//       MessageId: '970747fe-db1b-4c4d-8358-b38e32e8b536',
//       MD5OfMessageBody: '3e13b9a33cfb5cb4635920ed4a52c5f4'
//     }
//   ]
// }
