data "aws_subnets" "default_vpc_subnets_for_web_server" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default_vpc.id]
  }
}

data "aws_subnet" "default_vpc_subnet_ids_for_web_server" {
  for_each = toset(data.aws_subnets.default_vpc_subnets_for_web_server.ids)
  id = each.value
}

resource "aws_lb" "web_server_lb" {
  name = "webserverlb"
  internal = false
  load_balancer_type = "application"
  security_groups = [ "sg-e17dff87" ]
  # subnets = [ "subnet-84645fe0", "subnet-50d5f826", "subnet-bf5f63f9" ]
  subnets = [ for s in data.aws_subnet.default_vpc_subnet_ids_for_web_server : s.id ]
}