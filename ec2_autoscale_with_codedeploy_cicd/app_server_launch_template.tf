resource "aws_launch_template" "app_server_template" {
  name = "app_server_template"
  image_id = data.aws_ami.amazon_linux_2_ami.image_id
  instance_type = "t2.micro"
  key_name = "Rafaf-Ubuntu"
  user_data = "${filebase64("scripts/install_app_server.sh")}"
  vpc_security_group_ids = [ aws_security_group.app_server_sg.id ]
}