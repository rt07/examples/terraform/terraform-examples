resource "aws_autoscaling_group" "web_server_auto_scaling_group" {
  max_size = 1
  min_size = 1
  ### Subnet Ids
  vpc_zone_identifier = [ for s in data.aws_subnet.default_vpc_subnet_ids_for_web_server : s.id ]
  desired_capacity = 1
  launch_template {
    id = aws_launch_template.web_server_template.id
  }
}