resource "aws_launch_template" "web_server_template" {
  name = "web_server_template"
  image_id = data.aws_ami.amazon_linux_2_ami.image_id
  instance_type = "t2.micro"
  key_name = "Rafaf-Ubuntu"
  user_data = "${filebase64("scripts/install_web_server.sh")}"
  vpc_security_group_ids = [ aws_security_group.web_server_sg.id ]
}

#resource "aws_instance" "ec2_web_server" {
#  subnet_id = aws_subnet.public_subnet.id
#  ami = "ami-052f483c20fa1351a"
#  instance_type = "t2.micro"
#  vpc_security_group_ids = [aws_security_group.bravo.id]
#  key_name = "Rafaf-Ubuntu"
#  user_data = "${file("scripts/install_web_server.sh")}"
#  tags = {
#    name = "ec2_web_server"
#  }
#}
