data "aws_vpc" "default_vpc" {
  default = true
}

data "aws_ami" "amazon_linux_2_ami" {
  most_recent = true

  owners = [ "amazon" ]

  filter {
    name   = "architecture"
    values = [ "x86_64" ]
  }

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["al2023*"]
  }
}