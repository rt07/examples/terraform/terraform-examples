variable "aws_profile_name" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "maintainer" {
  type = string
}

variable "aws_account_id" {
  type = string
}