generate "versions" {
  path = "versions.tf"
  if_exists = "skip"

  contents = <<EOF
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.7.0"
    }
  }
}
EOF
}

generate "provider" {
  path = "providers.tf"
  if_exists = "skip"

  contents = <<EOF
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile_name

  default_tags {
    tags = {
      terraform = "yes"
      owner     = var.maintainer
    }
  }
}
EOF
}

generate "tfvars" {
  path = "terraform.tfvars"
  if_exists = "skip"

  contents = <<EOF
aws_profile_name = ""
aws_region =  ""
maintainer = "rafaftahsin"
aws_account_id = ""
EOF
}

generate "variables.tf" {
  path = "variables.tf"
  if_exists = "skip"

  contents = <<EOF
variable "aws_region" {
  type = string
}

variable "aws_profile_name" {
  type = string
}

variable "maintainer" {
  type = string
}
EOF
}

generate "output.tf" {
  path = "variables.tf"
  if_exists = "skip"

  contents = <<EOF
output "varname" {
  value = valuename
}
EOF
}

