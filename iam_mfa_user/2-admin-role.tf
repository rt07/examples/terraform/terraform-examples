### Assume Role Policy -> An assume role policy is a special policy associated with a role that
### controls which principals (users, other roles, AWS services, etc)
### can "assume" the role. Assuming a role means generating temporary credentials to act with the privileges granted
### by the access policies associated with that role.

resource "aws_iam_role" "iam_admin_role_with_mfa_restriction" {
  name               = "admin_mfa"
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "AWS" : "arn:aws:iam::${var.aws_account_id}:root"
        },
        "Action" : "sts:AssumeRole",
        "Condition" : {
          "Bool" : {
            "aws:MultiFactorAuthPresent" : "true"
          }
        }
      }
    ]
  })
  managed_policy_arns = ["arn:aws:iam::aws:policy/AdministratorAccess"]
}