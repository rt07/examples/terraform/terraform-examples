output "mfa_user_access_key" {
  value = aws_iam_access_key.mfa_user_keys.id
}

output "mfa_user_secret_key" {
  value     = aws_iam_access_key.mfa_user_keys.secret
  sensitive = true
}

output "mfa_user_console_password" {
  value = aws_iam_user_login_profile.mfa_user_console_login_profile.password
  sensitive = true
}

output "role_arn" {
  value = aws_iam_role.iam_admin_role_with_mfa_restriction.arn
}

