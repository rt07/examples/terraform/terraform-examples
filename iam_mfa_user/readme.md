### More about Assume Role Policy

https://stackoverflow.com/questions/44628380/terraform-assume-role-policy-similar-but-slightly-different-than-standard-ia

Path in IAM User/Role/Group: 

https://registry.terraform.io/providers/figma/aws-4-49-0/latest/docs/resources/iam_virtual_mfa_device

[Need to blog it]

aws-cli 2FA

https://stackoverflow.com/questions/34795780/how-to-use-mfa-with-aws-cli

Run Terraform with aws 2FA 

https://devops.stackexchange.com/questions/11368/how-do-i-run-terraform-with-aws-mfa


### --source-identity to keep log on who assumed the role 

rm -r ~/.aws/cli/cache

### ~/.aws/credentials 
Ref : https://aws.amazon.com/blogs/developer/assume-aws-iam-roles-with-mfa-using-the-aws-sdk-for-go/


### Use `aws configure --profile mfa_user` to configure mfa user in aws cli. Get the user credentials from terraform outputs. Sensitive outputs can be retrieved with `terraform output <sensitive_output_data>`. After configuration your `~/.aws/credentials` should contains this 

```
[mfa_user]
aws_access_key_id = ABCD$$$$$$$$
aws_secret_access_key = knklvdf093487jps/df\$$$$$$$$$$$$$$$$$$$$$$
```

### Lets create role profile

To create a role profile add `admin_role` profile section in `~/.aws/config`.

```
[profile admin_role]
role_arn = arn:aws:iam::<account_number>:role/admin_mfa
source_profile = mfa_user
mfa_serial = arn:aws:iam::<account_number>:mfa/mfa	
```

You can get role_arn from terraform output. Get MFA Serial from AWS Console.

### Now you can use aws cli with `admin_role` profile

```
aws s3 ls --profile admin_role
```

