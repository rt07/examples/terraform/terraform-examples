resource "aws_iam_user" "mfa_user" {
  # path = "/user_path_if_necessary/"
  name = "mfa_user"
}

data "aws_iam_policy_document" "sts_assume_role_policy_docuement" {
  statement {
    actions   = ["sts:AssumeRole"]
    resources = ["*"]
    effect    = "Allow"
    condition {
      test     = "Bool"
      values   = ["true"]
      variable = "aws:MultiFactorAuthPresent"
    }
  }
}

resource "aws_iam_policy" "sts_assume_role_policy" {
  name   = "sts_assume_role_policy"
  policy = data.aws_iam_policy_document.sts_assume_role_policy_docuement.json
}

resource "aws_iam_policy_attachment" "sts_assume_role_policy_attachment" {
  name       = "sts_assume_role_policy_attachment"
  policy_arn = aws_iam_policy.sts_assume_role_policy.arn
  users      = [aws_iam_user.mfa_user.name]
}

resource "aws_iam_access_key" "mfa_user_keys" {
  user = aws_iam_user.mfa_user.name
}

resource "aws_iam_user_login_profile" "mfa_user_console_login_profile" {
  user = aws_iam_user.mfa_user.name
}