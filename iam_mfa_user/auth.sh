#!/usr/bin/env bash

echo "totp is $1"

ROLE_ARN="arn:aws:iam::<account_number>:role/admin_role"
MFA_ARN="arn:aws:iam::<account_number>:mfa/mfa"

aws sts assume-role \
    --role-arn $ROLE_ARN \
    --role-session-name session-one \
    --serial-number $MFA_ARN \
    --token-code $1 > /tmp/sts.json

aws configure set aws_access_key_id $(cat /tmp/sts.json | jq -r '.Credentials.AccessKeyId') --profile terraform
aws configure set aws_secret_access_key $(cat /tmp/sts.json | jq -r '.Credentials.SecretAccessKey') --profile terraform
aws configure set aws_session_token $(cat /tmp/sts.json | jq -r '.Credentials.SessionToken') --profile terraform
aws configure set region "ap-southeast-1" --profile terraform

rm /tmp/sts.json

#AWS_ACCESS_KEY_ID
#AWS_SECRET_ACCESS_KEY
#AWS_DEFAULT_REGION
#AWS_ROLE_SESSION_NAME
#AWS_SESSION_TOKEN