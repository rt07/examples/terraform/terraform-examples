## repository module

module "public_ecr" {
  source = "terraform-aws-modules/ecr/aws"

  repository_name = "infltx"
  repository_type = "public"

#  repository_read_write_access_arns = ["arn:aws:iam::${var.aws_account_id}:root"]
#  repository_lifecycle_policy = jsonencode({
#    rules = [
#      {
#        rulePriority = 1,
#        description  = "Keep last 30 images",
#        selection = {
#          tagStatus     = "tagged",
#          tagPrefixList = ["v"],
#          countType     = "imageCountMoreThan",
#          countNumber   = 30
#        },
#        action = {
#          type = "expire"
#        }
#      }
#    ]
#  })
#
#  public_repository_catalog_data = {
#    description       = "Test Purpose Created"
#    operating_systems = ["Linux"]
#    architectures     = ["x86"]
#  }

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}