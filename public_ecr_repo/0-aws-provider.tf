provider "aws" {

  # for public repo, region must be us-east-1
  region  = "us-east-1"
  profile = var.aws_profile_name

  default_tags {
    tags = {
      terraform = "yes"
      owner     = var.maintainer
    }
  }
}